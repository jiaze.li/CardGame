﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CardGame
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        Card3DGame cardGame = new Card3DGame();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 10; i <= 120; i+=10)
            {
                cbSeconds.Items.Add(string.Format("{0,4} 秒", i));
            }
            cbSeconds.SelectedIndex = 0;

            cbMode.Items.Add("单张鬼牌");
            cbMode.Items.Add("四张鬼牌");
            cbMode.SelectedIndex = 0;

            cardGame.BackBrush = (ImageBrush)Resources["backImgBrush"];
            cardGame.JokerBrush = (ImageBrush)Resources["jokerImgBrush"];
            cardGame.NewGame();
            GridView.Children.Add(cardGame.GameGrid);
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            Card3DGameMode mode = cbMode.SelectedItem.ToString() == "单张鬼牌" ? Card3DGameMode.One : Card3DGameMode.Four;
            cardGame.ChangeMode(mode);
            cardGame.RememberTime = int.Parse(cbSeconds.SelectedItem.ToString().Substring(0, 4));
            cardGame.NewGame(); 
            cardGame.StartGame();
            btnStart.IsEnabled = false;
            btnNew.IsEnabled = true;
            cbMode.IsEnabled = false;
            cbSeconds.IsEnabled = false;
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            cardGame.NewGame(); 
            btnStart.IsEnabled = true;
            btnNew.IsEnabled = false;
            cbMode.IsEnabled = true;
            cbSeconds.IsEnabled = true;
        }
    }
}
